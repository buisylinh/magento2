<?php

namespace SmartOsc\Article\Block;

use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\Registry;
use \SmartOsc\Article\Model\Post;
use \SmartOsc\Article\Model\PostFactory;
use \SmartOsc\Article\Controller\Post\View as ViewAction;
use \Magento\Framework\UrlInterface;

class View extends Template
{
    /**
     * Core registry
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Post
     * @var null|Post
     */
    protected $_post = null;

    /**
     * PostFactory
     * @var null|PostFactory
     */
    protected $_postFactory = null;

    /**
     * Constructor
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PostFactory $postCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PostFactory $postFactory,
        UrlInterface $urlInterface,
        array $data = []
    ) {
        $this->_postFactory = $postFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_urlInterface = $urlInterface;
        parent::__construct($context, $data);
    }

    /**
     * Lazy loads the requested post
     * @return Post
     * @throws LocalizedException
     */
    public function getPost()
    {

        if ($this->_post === null) {
            /** @var Post $post */

            $post = $this->_postFactory->create();

            $post->load($this->_getPostId());

            if (!$post->getId()) {
                           
                throw new LocalizedException(__('die ngắt'));
            }
            $this->_post = $post;
        }
        return $this->_post;
    }
    
    /**
     * Retrieves the post id from the registry
     * @return int
     */
    protected function _getPostId()
    {
        // echo $this->_coreRegistry->registry(
        //     ViewAction::REGISTRY_KEY_POST_ID
        // );die;
        return (int) $this->_coreRegistry->registry(
            ViewAction::REGISTRY_KEY_POST_ID
        );
    }
    public function getbaseUrl(){
        return $this->_urlInterface->getBaseUrl();
    }
}