<?php

namespace SmartOsc\Article\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \SmartOsc\Article\Model\ResourceModel\Post\Collection as PostCollection;
use \SmartOsc\Article\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use \SmartOsc\Article\Model\Post;

class Posts extends Template
{
    /**
     * CollectionFactory
     * @var null|CollectionFactory
     */
    protected $_postCollectionFactory = null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PostCollectionFactory $postCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        PostCollectionFactory $postCollectionFactory,
        array $data = []
    ) {
        $this->_postCollectionFactory = $postCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return Post[]
     */
    public function getPosts()
    {  
         /** @var PostCollection $postCollection */
        // $postCollection = $this->_postCollectionFactory->create();
        // $postCollection->load();
        // return $postCollection->getItems();
             /** @var PostCollection $postCollection */

        $page = ($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $limit = ($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 1;

        $postCollection = $this->_postCollectionFactory->create();
        $postCollection->addFieldToSelect('*')->load();
        $postCollection->setPageSize($limit);
        $postCollection->setCurPage($page);

        return $postCollection;
    }

    protected function _prepareLayout(){
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('SmartOsc'));
     

        if ($this->getPosts()){
            $pager = $this->getLayout()->createBlock('Magento\Theme\Block\Html\Pager', 'article.blog.pager')
                                    ->setAvailableLimit(array(1=>1,10=>10))
                                    ->setShowPerPage(true)
                                    ->setCollection($this->getPosts());

            $this->setChild('pager', $pager);


            $this->getPosts()->load();

        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * For a given post, returns it's url
     * @param Post $post
     * @return string
     */
    public function getPostUrl(Post $post) 
    {
        return '/article/post/view/id/' . $post->getId();
    }
}