<?php
namespace SmartOsc\Article\Model\ResourceModel\Post;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('SmartOsc\Article\Model\Post', 'SmartOsc\Article\Model\ResourceModel\Post');
    }
}
